from django import template
from carrito.models import Carrito

register = template.Library()

@register.filter
def lower(value):
    return value[0:180] + '...'

@register.filter
def subtotal(value, articulo_id):

    unidades = Carrito.objects.get(articulo_id=articulo_id).cantidad

    return value * unidades