from django.contrib import admin
from .models import Sistema, Envio, Metodo_pago, CarouselConfig, BackgroundConfig
from .models import LogoConfig, TitleConfig, DescriptionConfig

"""
class JuegoAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'portada_img', 'consola', 'precio', 'stock')
    list_filter = ('consola', 'precio', 'stock')

    def portada_img(self, obj):
        return format_html('<img src={} height="75" />', obj.portada.url)

admin.site.register(Juego, JuegoAdmin)
"""

admin.site.register(Sistema)
admin.site.register(Envio)
admin.site.register(Metodo_pago)
admin.site.register(CarouselConfig)
admin.site.register(BackgroundConfig)
admin.site.register(LogoConfig)
admin.site.register(TitleConfig)
admin.site.register(DescriptionConfig)