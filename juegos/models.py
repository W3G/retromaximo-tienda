from django.db import models
from core.models import Sistema

class Juego(models.Model):
    """ Juegos repro a la venta """

    titulo = models.CharField(max_length=250, verbose_name='Título')
    consola = models.ForeignKey(Sistema, on_delete=models.CASCADE, verbose_name='Consola')
    descripcion = models.TextField(verbose_name='Descripción')
    precio = models.FloatField(verbose_name='Precio (€)')
    stock = models.IntegerField(verbose_name='Stock disponible')
    portada = models.ImageField(upload_to='portadas/', verbose_name='Foto portada')

    imagen_uno = models.ImageField(upload_to='juegos/', verbose_name='Imagen 1', blank=True)
    imagen_dos = models.ImageField(upload_to='juegos/', verbose_name='Imagen 2', blank=True)
    imagen_tres = models.ImageField(upload_to='juegos/', verbose_name='Imagen 3', blank=True)
    imagen_cuatro = models.ImageField(upload_to='juegos/', verbose_name='Imagen 4', blank=True)
    imagen_cinco = models.ImageField(upload_to='juegos/', verbose_name='Imagen 5', blank=True)
    imagen_seis = models.ImageField(upload_to='juegos/', verbose_name='Imagen 6', blank=True)
    imagen_siete = models.ImageField(upload_to='juegos/', verbose_name='Imagen 7', blank=True)
    imagen_ocho = models.ImageField(upload_to='juegos/', verbose_name='Imagen 8', blank=True)
    imagen_nueve = models.ImageField(upload_to='juegos/', verbose_name='Imagen 9', blank=True)
    imagen_diez = models.ImageField(upload_to='juegos/', verbose_name='Imagen 10', blank=True)

    create_date = models.DateTimeField(auto_now_add = True)
    update_date = models.DateTimeField(auto_now = True)
    
    class Meta:
        verbose_name = 'Juego'
        verbose_name_plural = 'Listado de juegos'
        ordering = ['-create_date', ]

    def __str__(self):
        return self.titulo