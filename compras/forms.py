from django import forms
from .models import Compra
from carrito.models import Carrito 
from core.models import Metodo_pago, Envio


class CompraForm(forms.ModelForm):
    """ Registra un pedido en la web """

    nombre = forms.CharField(
        label='Nombre',
        max_length=255,
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        )
    )

    apellidos = forms.CharField(
        label='Apellidos',
        max_length=255,
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        )
    )

    direccion = forms.CharField(
        label='Direccion',
        max_length=255,
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        )
    )

    codigo_postal = forms.CharField(
        label='Código Postal',
        max_length=5,
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        )
    )

    localidad = forms.CharField(
        label='Localidad',
        max_length=255,
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        )
    )

    provincia = forms.CharField(
        label='Provincia',
        max_length=255,
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        )
    )

    telefono = forms.CharField(
        label='Teléfono',
        max_length=9,
        required=True,
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        )
    )

    email = forms.CharField(
        label='Email',
        max_length=255,
        required=True,
        widget=forms.EmailInput(
            attrs={'class': 'form-control'}
        )
    )

    productos = forms.CharField(
        required=True,
        max_length=255,
        widget=forms.HiddenInput(
            attrs={'class': 'form-control', 'style':'hidden:true;'}
        )
    )

    total = forms.DecimalField(
        required=True,
        widget=forms.HiddenInput(
            attrs={'class': 'form-control'}
        )
    )

    metodo_pago = forms.ChoiceField(
        label='Método de pago',
        choices= [('','')],
        required=True,
        widget=forms.Select(
            attrs={'class': 'form-select', 'id': 'metodo_pago'}
        )
    )

    envio = forms.ChoiceField(
        label='Envío',
        choices= [('','')],
        required=True,
        widget=forms.Select(
            attrs={'class': 'form-select', 'id': 'envio'}
        )
    )

    class Meta:
        model = Compra 
        fields = (
            'nombre', 
            'apellidos', 
            'direccion', 
            'codigo_postal', 
            'localidad', 
            'provincia', 
            'telefono', 
            'email',
            'productos',
            'total',
            'metodo_pago',
            'envio'
            )

    def __init__(self, *args, **kwargs):
        self.carrito = kwargs.pop('carrito', None)
        self.total = kwargs.pop('total', None)


        super().__init__(*args, **kwargs)
        
        if self.carrito:
            carrito = ['Articulo: ' + c.articulo.titulo + ' | Cantidad: ' + str(c.cantidad) + ' <br/>' for c in self.carrito]
            self.fields['productos'].initial = ''.join(carrito)
        
        if self.total:
            self.fields['total'].initial = self.total
        
        metodos = Metodo_pago.objects.all()
        if metodos: 
            self.fields['metodo_pago'].choices += [(x.id, x.tipo_pago) for x in metodos]

        tipo_envio = Envio.objects.all()
        if tipo_envio: 
            self.fields['envio'].choices += [(x.id, x.nombre + ': ' + str(x.importe) + ' €' + x.tiempo) for x in tipo_envio]
        



