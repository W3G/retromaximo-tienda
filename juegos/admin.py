from django.contrib import admin
from .models import Juego 
from django.utils.html import format_html

class JuegoAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'portada_img', 'consola', 'precio', 'stock')
    list_filter = ('consola', 'precio', 'stock')

    def portada_img(self, obj):
        return format_html('<img src={} height="75" />', obj.portada.url)

admin.site.register(Juego, JuegoAdmin)

