from django.db import models
from juegos.models import Juego

class Carrito(models.Model):
    """ Carrito de la compra """

    ip = models.CharField(max_length=50, verbose_name='IP Cliente')
    articulo = models.ForeignKey(Juego, on_delete=models.CASCADE, verbose_name='Artículo')
    cantidad = models.IntegerField(verbose_name='Cantidad')


class EnvioMetodoPago(models.Model):
    """ Tabla temporal que registra los importes de envio y metodo de pago """

    ip = models.CharField(max_length=50)
    metodo_pago = models.FloatField(blank=True)
    envio = models.FloatField(blank=True)

    