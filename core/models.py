from django.db import models

class Sistema(models.Model):
    """ Sistemas de juego o consolas """

    nombre = models.CharField(max_length=200, verbose_name='Consola')
    imagen = models.ImageField(upload_to='consolas/', blank=True)

    create_date = models.DateTimeField(auto_now_add = True)
    update_date = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = 'Consola'
        verbose_name_plural = 'Panel de Consolas'

    def __str__(self):
        return self.nombre 


class Envio(models.Model):
    """ Tipo de envío """

    nombre = models.CharField(max_length=200, verbose_name='Compañía')
    logo = models.ImageField(upload_to='mensajeria/', verbose_name='Logotipo', blank=True)
    tiempo = models.CharField(max_length=200, verbose_name='Tiempo de Envío', blank=True)
    extension = models.CharField(max_length=200, verbose_name='Territorios', blank=True)
    importe = models.FloatField(verbose_name='Coste del envío')

    create_date = models.DateTimeField(auto_now_add = True)
    update_date = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = 'Envío'
        verbose_name_plural = 'Tipos de envios'

    def __str__(self):
        return self.nombre


class Metodo_pago(models.Model):
    """ Modalidad de pago """

    tipo_pago = models.CharField(max_length=250, verbose_name='Tipo de pago')
    logo = models.ImageField(upload_to='tipo_pagos/', verbose_name='Logo', blank=True)
    observaciones = models.TextField(verbose_name='Observaciones')

    create_date = models.DateTimeField(auto_now_add = True)
    update_date = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = 'Metodo de pago'
        verbose_name_plural = 'Métodos de pago'

    def __str__(self):
        return self.tipo_pago


class CarouselConfig(models.Model):
    """ Configuración del Carousel de imágenes """

    orden = models.IntegerField(verbose_name='Posición de la imagen')
    imagen = models.ImageField(upload_to='carousel/', verbose_name='Imagen (Dimensiones recomendadas: 1290px x 350px)', blank=True)

    create_date = models.DateTimeField(auto_now_add = True)
    update_date = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = 'Carousel'
        verbose_name_plural = 'Carousel de imágenes'
        ordering = ['orden', ]

    def __str__(self):
        return str(self.orden)


class BackgroundConfig(models.Model):
    """ Configurar imagen de fondo """

    titulo = models.CharField(max_length=150, verbose_name='Nombre del fondo')
    imagen = models.ImageField(upload_to='fondo/', verbose_name='Imagen (recomendado 1920x1080px)', blank=True)

    create_date = models.DateTimeField(auto_now_add = True)
    update_date = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = 'Fondo'
        verbose_name_plural = 'Imagen de fondo'

    def __str__(self):
        return self.titulo


class LogoConfig(models.Model):
    """ Logo del sitio """

    titulo = models.CharField(max_length=150, verbose_name='Nombre del fondo')
    imagen = models.ImageField(upload_to='logo/', verbose_name='Imagen (recomendado 1920x1080px)', blank=True)

    create_date = models.DateTimeField(auto_now_add = True)
    update_date = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = 'Logo'
        verbose_name_plural = 'Logo del sitio'

    def __str__(self):
        return self.titulo


class TitleConfig(models.Model):
    """ Título del sitio web """

    titulo = models.CharField(max_length=150, verbose_name='Título del sitio web')

    create_date = models.DateTimeField(auto_now_add = True)
    update_date = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = 'Título'
        verbose_name_plural = 'Título del sitio web'

    def __str__(self):
        return self.titulo


class DescriptionConfig(models.Model):
    """ Título del sitio web """

    descripcion = models.TextField(verbose_name='Descripción del sitio web')

    create_date = models.DateTimeField(auto_now_add = True)
    update_date = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = 'Descripción'
        verbose_name_plural = 'Descripción del sitio'

    def __str__(self):
        return self.descripcion