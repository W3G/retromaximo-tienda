from django.apps import AppConfig

class JuegosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'juegos'

    verbose_name = 'Catálogo'
