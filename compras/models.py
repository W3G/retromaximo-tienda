from django.db import models
from core.models import Metodo_pago, Envio

class Compra(models.Model):
    """ Pedidos realizados a la web """

    # DATOS DEL CLIENTE:
    nombre = models.CharField(max_length=250, verbose_name='Nombre')
    apellidos = models.CharField(max_length=250, verbose_name='Apellidos')
    direccion = models.CharField(max_length=250, verbose_name='Dirección')
    codigo_postal = models.CharField(max_length=5, verbose_name='Código Postal')
    localidad = models.CharField(max_length=250, verbose_name='Localidad')
    provincia = models.CharField(max_length=100, verbose_name='Provincia')
    telefono = models.CharField(max_length=9, verbose_name='Teléfono')
    email = models.EmailField(max_length=250, verbose_name='Email')

    # LINEA DE PEDIDO
    productos = models.TextField(verbose_name='Productos')
    total = models.FloatField(verbose_name='Importe Total (€)')
    metodo_pago = models.CharField(max_length=200, verbose_name='Método de pago')
    envio = models.CharField(max_length=200, verbose_name='Tipo de Envío')

    # ESTADO
    pago_realizado = models.BooleanField(default=False, verbose_name='Pago realizado')
    enviado = models.BooleanField(default=False, verbose_name='Enviado')
    recibido = models.BooleanField(default=False, verbose_name='Recibido')

    fecha_pedido = models.DateTimeField(auto_now_add = True, verbose_name='Fecha del pedido')

    update_date = models.DateTimeField(auto_now = True)

    class Meta:
        verbose_name = 'Pedido'
        verbose_name_plural = 'Listado de pedidos'

    def __str__(self):
        return 'Pedido nº ' + str(self.id)
