# Generated by Django 3.2.9 on 2021-11-28 18:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('compras', '0004_compra_envio'),
    ]

    operations = [
        migrations.AlterField(
            model_name='compra',
            name='envio',
            field=models.CharField(max_length=200, verbose_name='Tipo de Envío'),
        ),
        migrations.AlterField(
            model_name='compra',
            name='metodo_pago',
            field=models.CharField(max_length=200, verbose_name='Método de pago'),
        ),
    ]
