// get Elements:
function getElements(){
    /* Recuperar todos los elementos y añadirles un listener  */

    // Seleccionar consola:
    menu = document.getElementsByClassName('menu');
    try{
        for(key in menu){
            menu[key].addEventListener('click', (e)=>{
                let consola = e.currentTarget.getAttribute('id');
                getCategory(consola);
            })
        }
    }catch(TypeError){
        // console.log('recorrido');
    }

    // Abrir Modal videojuego:
    videojuego = document.getElementById('videojuego');
    videojuego.addEventListener('show.bs.modal', function (event) {
        // Button that triggered the modal
        var button = event.relatedTarget
        // Extract info from data-bs-* attributes
        var gameId = button.getAttribute('data-juego')
        // If necessary, you could initiate an AJAX request here
        // and then do the updating in a callback.
        //
        // Update the modal's content.

        juego = getGame(gameId);
    });

    // Abrir Modal carrito:
    carrito = document.getElementById('carritoModal');
    carrito.addEventListener('show.bs.modal', function (event) {
        getCarrito();
    });
      
    // elegir metodo de pago:
    metodoPago = document.getElementById('metodo_pago')
    metodoPago.addEventListener('change', (e)=>{
        var id = e.currentTarget.value;
        reprintImport('metodo_pago', id);
    });

    // elegir envio:
    envio = document.getElementById('envio')
    envio.addEventListener('change', (e)=>{
        var id = e.currentTarget.value;
        reprintImport('envio', id);
    });
}

function getCategory(consola){
    /* Recupera productos filtrados por consola */

    url = window.location.href.replace('#', '');
    // recuperar datos:
    fetch(url + 'catalogo/' + consola)
    .then(function(response) {
        response.text().then(cargarDatos);
    })
    .catch(function() {
        console.log('ERROR: getCategory');
    });
}

function cargarDatos(data){
    /* Pinta productos filtrados por consola */

    let catalogo = document.getElementById('main');
    catalogo.innerHTML = data
}

function getGame(juego){
    /* Recupera la información de un videojuego */

    url = window.location.href.replace('#', '');
    // recuperar datos:
    fetch(url + 'juego/' + juego)
    .then(function(response) {
        response.text().then(cargarJuego);
    })
    .catch(function() {
        console.log('ERROR: getCategory');
    });
}

function cargarJuego(juego){
    /* Pinta la información de un videojuego */

    var modalBody = document.getElementById('modalBody');

    modalBody.innerHTML = juego

    // Botón añadir articulo:
    agregarArticulo = document.getElementById('agregarArticulo');
    if(agregarArticulo){
        agregarArticulo.addEventListener('click', ()=>{
            juego = agregarArticulo.dataset.juego
            guardarCarrito(juego);
        });
    }
}

function getCarrito(){
    /* Recupera la información del carrito  */
    
    url = window.location.href.replace('#', '');
    // recuperar datos:
    fetch(url + 'carrito/')
    .then(function(response) {
        response.text().then(cargarCarrito);
    })
    .catch(function() {
        console.log('ERROR: abrirCarrito');
    });
}

function cargarCarrito(carro){
    /* Recupera la información del carrito  */
    
    // pintar tabla de elementos
    var modalBody = document.getElementById('modalBodyCarrito');
    modalBody.innerHTML = carro;
    
    // Modificar cantidad de productos:
    cantidad = document.getElementsByClassName('cantidad');
    try{
        for(key in cantidad){
            cantidad[key].addEventListener('input', (e)=>{
                var cantidad = e.currentTarget.value;
                var id = e.currentTarget.dataset.id;
                actualizarCarrito(id, cantidad);
            })
        }
    }catch(TypeError){
        // console.log('recorrido');
    }

    // borrar articulo del carrito:
    borrar = document.getElementsByClassName('basura');
    try{
        for(key in borrar){
            borrar[key].addEventListener('click', (e)=>{
                var id = e.currentTarget.dataset.id;
                borrarArticulo(id);
            })
        }
    }catch(TypeError){
        // console.log('recorrido');
    }
}

function guardarCarrito(juego){
    /* Guardar juego en el carrito */

    url = window.location.href.replace('#', '');
    // recuperar datos:
    fetch(url + 'guardarCarrito/' + juego)
    .then(function(response) {
        response.text().then(guardadoExito);
    })
    .catch(function() {
        console.log('ERROR: guardarCarrito()');
    });
}

function guardadoExito(){
    /* Cargar notificación de guardado */
    agregar = document.getElementById('agregado');
    agregar.classList.remove('hide');
    agregar.style.cssText = 'position:fixed; width:100%; z-index:1000;';
    agregar.classList.add('show');
    window.setTimeout(function(){ 
        agregar.classList.add('hide');
        agregar.classList.remove('show');
     }, 2000);
     window.setTimeout(function(){ 
        agregar.style.cssText = 'position:fixed; width:100%; z-index:-1000;';
     }, 2500);
}

function actualizarCarrito(id, cantidad){
    /* Actualizar el carrito */

    url = window.location.href.replace('#', '');
    // recuperar datos:
    fetch(url + 'actualizarCarrito/' + id + '/' + cantidad)
    .then(function(response) {
        response.text().then(cargarCarrito);
    })
    .catch(function() {
        console.log('ERROR: actualizarCarrito()');
    });
}

function borrarArticulo(id){
    /* Borra un artículo del carrito */

    url = window.location.href.replace('#', '');
    // recuperar datos:
    fetch(url + 'borrarArticulo/' + id)
    .then(function(response) {
        response.text().then(cargarCarrito);
    })
    .catch(function() {
        console.log('ERROR: actualizarCarrito()');
    });
}

function reprintImport(value, id){
    /* Actualizar total de resumen */

    url = window.location.href.replace('#', '');
    url = url.replace('/resumen', '');
    if (id.length <= 0){
        id = 0
    }
    // recuperar datos:
    fetch(url + 'actualizarResumen/' + value + '/' + id)
    .then(function(response) {
        response.text().then(cargarResumen);
    })
    .catch(function() {
        console.log('ERROR: reprintImport()');
    });
}

function cargarResumen(resumen){

    // pintar tabla de elementos
    var totalResumen = document.getElementById('totalResumen');
    totalResumen.innerHTML = resumen;
}

// initial:
document.addEventListener('DOMContentLoaded', ()=>{
    getElements();
});