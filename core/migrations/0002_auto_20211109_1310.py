# Generated by Django 3.2.9 on 2021-11-09 12:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Envio',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200, verbose_name='Compañía')),
                ('logo', models.ImageField(blank=True, upload_to='mensajeria/', verbose_name='Logotipo')),
                ('tiempo', models.CharField(blank=True, max_length=200, verbose_name='Tiempo de Envío')),
                ('extension', models.CharField(blank=True, max_length=200, verbose_name='Territorios')),
                ('importe', models.FloatField(verbose_name='Coste del envío')),
                ('create_date', models.DateTimeField(auto_now_add=True)),
                ('update_date', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Envío',
                'verbose_name_plural': 'Configuración de envios',
            },
        ),
        migrations.AlterModelOptions(
            name='sistema',
            options={'verbose_name': 'Consola', 'verbose_name_plural': 'Panel de Consolas'},
        ),
    ]
