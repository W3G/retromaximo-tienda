from django.shortcuts import render
from django.http import HttpResponse
from core.models import Sistema, CarouselConfig, TitleConfig, DescriptionConfig, BackgroundConfig, LogoConfig, Metodo_pago, Envio
from juegos.models import Juego
from carrito.models import Carrito, EnvioMetodoPago
from compras.forms import CompraForm
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template

def index(request):
    """ Página principal """

    # para cuando haga el carrito:
    ip = request.META.get('REMOTE_ADDR')

    sistemas = Sistema.objects.all()
    carousel = CarouselConfig.objects.all()
    juegos = Juego.objects.all()
    title = TitleConfig.objects.all()
    if title:
        title = title[0]
    description = DescriptionConfig.objects.all()
    if description:
        description = description[0]
    background = BackgroundConfig.objects.all()
    if background:
        background = background[0]
    logo = LogoConfig.objects.all()
    if logo:
        logo = logo[0]
    metodos_pago = Metodo_pago.objects.all()


    # Filtrar por consola via url?

    return render(request, 'core/index.html', {
        'sistemas': sistemas, 
        'juegos': juegos, 
        'carousel': carousel,
        'title': title,
        'description': description,
        'background': background,
        'logo': logo,
        'metodos_pago': metodos_pago
        })


def get_catalogo(request, consola):
    """ devuelve una promesa con la lista de videojuegos según su consola """

    juegos = Juego.objects.filter(consola__nombre=consola)

    return render(request, 'juegos/catalogo.html', {'juegos': juegos, 'consola': consola})


def get_juego(request, id_juego):
    """ devuelve un html para rellenar el modal que muestra los juegos """

    juego = Juego.objects.get(id=id_juego)

    return render(request, 'juegos/juego.html', {'juego': juego})


def get_carrito(request):
    """ devuelve un html para rellenar el modal de carrito """

    ip = request.META.get('REMOTE_ADDR')

    carrito = Carrito.objects.filter(ip=ip)

    total = list(map(lambda c: c.articulo.precio * c.cantidad,carrito))
    total = sum(total)

    return render(request, 'carrito/list.html', {'carrito': carrito, 'total': total})


def guardar_carrito(request, id_juego):
    """ Guarda un artículo en el carrito """

    ip = request.META.get('REMOTE_ADDR')

    carrito = Carrito.objects.filter(ip=ip, articulo_id=id_juego)

    if carrito:
        cantidad = carrito[0].cantidad + 1
        carrito.update(cantidad=cantidad)
    else:
        Carrito.objects.create(ip=ip, cantidad=1, articulo_id=id_juego)

    return HttpResponse('ok')


def actualizar_carrito(request, id, cantidad):
    """ Actualiza un articulo en el carrito """

    Carrito.objects.filter(id=id).update(cantidad=cantidad)

    ip = request.META.get('REMOTE_ADDR')

    carrito = Carrito.objects.filter(ip=ip)
    total = list(map(lambda c: c.articulo.precio * c.cantidad,carrito))
    total = sum(total)

    return render(request, 'carrito/list.html', {'carrito': carrito, 'total':total})

def borrar_articulo(request, id):
    """ Borrar un artículo del carrito """

    Carrito.objects.filter(id=id).delete()
    
    ip = request.META.get('REMOTE_ADDR')

    carrito = Carrito.objects.filter(ip=ip)
    total = list(map(lambda c: c.articulo.precio * c.cantidad,carrito))
    total = sum(total)

    return render(request, 'carrito/list.html', {'carrito': carrito, 'total':total})


def resumen(request):
    """ Abre la ventana de resumen para confirmar pedido """

    sistemas = Sistema.objects.all()
    juegos = Juego.objects.all()
    title = TitleConfig.objects.all()
    if title:
        title = title[0]
    description = DescriptionConfig.objects.all()
    if description:
        description = description[0]
    background = BackgroundConfig.objects.all()
    if background:
        background = background[0]
    logo = LogoConfig.objects.all()
    if logo:
        logo = logo[0]
    metodos_pago = Metodo_pago.objects.all()

    ip = request.META.get('REMOTE_ADDR')

    carrito = Carrito.objects.filter(ip=ip)

    total = list(map(lambda c: c.articulo.precio * c.cantidad,carrito))
    total = sum(total)

    form = CompraForm(carrito=carrito, total=total)

    if request.POST:
        print(request.POST)
        form = CompraForm(request.POST)
        metodo_pago = Metodo_pago.objects.filter(id=request.POST.get('metodo_pago', None)).first()
        envio = Envio.objects.filter(id=request.POST.get('envio', None)).first()
        total = request.POST.get('totalHide', None).replace(',', '.')

        if form.is_valid():
            registro = form.save(commit=False)
            
            registro.metodo_pago = metodo_pago
            registro.envio = envio
            registro.total = float(total)
            
            registro.save()

            
            print('ID REGISTRO:')
            print('/admin/compras/compra/' + str(registro.id) + '/change/')

            send_user_mail(registro)
            Carrito.objects.filter(ip=ip).delete()

            return render(request, 'carrito/fin.html', {
                'carrito': carrito, 
                'total': total,
                'sistemas': sistemas,
                'juegos': juegos,
                'title': title,
                'description': description,
                'background': background,
                'logo': logo,
                'metodos_pago': metodos_pago,
                'form': form
                })

    return render(request, 'carrito/resumen.html', {
        'carrito': carrito, 
        'total': total,
        'sistemas': sistemas,
        'juegos': juegos,
        'title': title,
        'description': description,
        'background': background,
        'logo': logo,
        'metodos_pago': metodos_pago,
        'form': form
        })

def actualizar_resumen(request, value, id):
    """ Actualizar resumen del pedido """

    ip = request.META.get('REMOTE_ADDR')

    resumen = EnvioMetodoPago.objects.filter(ip=ip).first()

    if resumen is None:
        EnvioMetodoPago.objects.create(ip=ip, metodo_pago=0, envio=0)

    if value == 'envio':
        importe = Envio.objects.filter(id=id).first()
        try:
            EnvioMetodoPago.objects.filter(ip=ip).update(envio=importe.importe)
        except:
            EnvioMetodoPago.objects.filter(ip=ip).update(envio=0)
    if value == 'metodo_pago':
        importe = Metodo_pago.objects.filter(id=id).first()
        try:
            EnvioMetodoPago.objects.filter(ip=ip).update(metodo_pago=importe.importe)
        except:
            EnvioMetodoPago.objects.filter(ip=ip).update(metodo_pago=0)

    resumen = EnvioMetodoPago.objects.filter(ip=ip).first()

    carrito = Carrito.objects.filter(ip=ip)

    total = list(map(lambda c: c.articulo.precio * c.cantidad,carrito))
    total = sum(total) + resumen.envio

    return render(request, 'carrito/_partials/total.html', {'total':total})


def send_user_mail(user):
    subject = 'Pedido RetroMáximo: #' + str(user.id)
    template = get_template('carrito/email.html')

    content = template.render({
        'user': user,
    })

    message = EmailMultiAlternatives(subject, #Titulo
                                    '',
                                    settings.EMAIL_HOST_USER, #Remitente
                                    [user.email]) #Destinatario

    message.attach_alternative(content, 'text/html')
    message.send()
    send_admin_mail(user)

def send_admin_mail(user):
    subject = 'Pedido RetroMáximo: #' + str(user.id)
    template = get_template('carrito/email_admin.html')

    content = template.render({
        'user': user,
        'url': '/admin/compras/compra/' + str(user.id) + '/change/'
    })

    message = EmailMultiAlternatives(subject, #Titulo
                                    '',
                                    settings.EMAIL_HOST_USER, #Remitente
                                    [settings.EMAIL_HOST_USER]) #Destinatario

    message.attach_alternative(content, 'text/html')
    message.send()