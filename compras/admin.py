from django.contrib import admin
from .models import Compra 
from django.utils.html import format_html

class CompraAdmin(admin.ModelAdmin):
    list_display = ('orden', 'fecha_pedido', 'nombre', 'apellidos', 'direccion', 
    'codigo_postal', 'localidad', 'provincia', 'telefono', 'email', 'total', 
    'pago_realizado', 'enviado', 'recibido')

    readonly_fields = ('fecha_pedido', )

    search_fields = ('id', 'fecha_pedido', 'nombre', 'apellidos', 'total')

    def orden(self, obj):
        return 'pedido nº ' + str(obj.id)

admin.site.register(Compra, CompraAdmin)
