"""retromaximo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from core import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('catalogo/<consola>', views.get_catalogo, name='catalogo'),
    path('juego/<int:id_juego>', views.get_juego, name='juego'),
    path('carrito/', views.get_carrito, name='carrito'),
    path('guardarCarrito/<int:id_juego>', views.guardar_carrito, name='guardar_carrito'),
    path('actualizarCarrito/<int:id>/<cantidad>', views.actualizar_carrito, name="actualizar_carrito"),
    path('borrarArticulo/<int:id>', views.borrar_articulo, name="borrar_articulo"),
    path('resumen/', views.resumen, name="resumen"),
    path('actualizarResumen/<value>/<int:id>', views.actualizar_resumen, name="actualizar_resumen")
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

admin.site.site_header = 'RETROMAXIMO'

admin.site.index_title = 'Panel de Administración'

admin.site.site_title = 'Juegos Repro a la carta'
